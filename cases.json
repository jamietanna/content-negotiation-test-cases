{
  "cases": [
    {
      "name": "when a single supported type",
      "supported": [
        "application/json"
      ],
      "acceptHeader": "application/json",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "when any media type is supported",
      "supported": [
        "*/*"
      ],
      "acceptHeader": "application/json",
      "negotiated": [
        "*/*"
      ]
    },
    {
      "name": "when any media type is supported",
      "supported": [
        "*/*"
      ],
      "acceptHeader": "text/plain",
      "negotiated": [
        "*/*"
      ]
    },
    {
      "name": "when a wildcard is configured, it takes priority over other configured types",
      "supported": [
        "*/*",
        "application/json"
      ],
      "acceptHeader": "application/json",
      "negotiated": [
        "*/*"
      ]
    },
    {
      "name": "no matches are found when ",
      "supported": [
        "application/json"
      ],
      "acceptHeader": "text/plain",
      "negotiated": []
    },
    {
      "name": "multiple matches are found, when a wildcard type accept header",
      "supported": [
        "text/plain",
        "application/vnd.api+json"
      ],
      "acceptHeader": "*/*",
      "negotiated": []
    },
    {
      "name": "multiple matches are found, when a wildcard subtype accept header",
      "supported": [
        "application/json",
        "application/vnd.api+json"
      ],
      "acceptHeader": "application/*",
      "negotiated": []
    },
    {
      "name": "multiple matches are found, when a wildcard subtype suffix in the accept header",
      "supported": [
        "application/json",
        "application/vnd.api+json"
      ],
      "acceptHeader": "application/*+json",
      "negotiated": []
    },
    {
      "name": "performs quality-based negotiation, when no quality is set, q=1 is implied",
      "supported": [
        "application/json",
        "application/vnd.api+json"
      ],
      "acceptHeader": "application/json",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "performs quality-based negotiation, when quality is set, with one at q=1, it is abided by",
      "supported": [
        "application/json",
        "application/vnd.api+json"
      ],
      "acceptHeader": "application/json; q=0.3, application/vnd.api+json",
      "negotiated": [
        "application/vnd.api+json"
      ]
    },
    {
      "name": "performs quality-based negotiation, when quality is set, with both at fractional values, it is abided by",
      "supported": [
        "application/json",
        "application/vnd.api+json"
      ],
      "acceptHeader": "application/json; q=0.3, application/vnd.api+json;q=0.9",
      "negotiated": [
        "application/vnd.api+json"
      ]
    },
    {
      "name": "performs specificity-based negotiation",
      "supported": [
        "application/json"
      ],
      "acceptHeader": "application/*",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "ignores non-quality parameters when negotiating, with parameter on server-side",
      "supported": [
        "application/json;v=1"
      ],
      "acceptHeader": "application/json",
      "negotiated": [
        "application/json; v=1"
      ]
    },
    {
      "name": "ignores non-quality parameters when negotiating, with parameter on client-side",
      "supported": [
        "application/json"
      ],
      "acceptHeader": "application/json;v=1",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "does not return quality value in negotiated value, when q=1",
      "supported": [
        "application/json"
      ],
      "acceptHeader": "application/json;q=1",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "does not return quality value in negotiated value, when q=0.1",
      "supported": [
        "application/json"
      ],
      "acceptHeader": "application/json;q=0.1",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "suffixes can be used, when only one type",
      "supported": [
        "application/json"
      ],
      "acceptHeader": "application/*+json",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "suffixes can be used, when only one type can match",
      "supported": [
        "application/xml",
        "application/json"
      ],
      "acceptHeader": "application/*+json",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "suffixes can be used, when multiple types that can match",
      "supported": [
        "application/xml",
        "application/json"
      ],
      "acceptHeader": "application/*+json",
      "negotiated": [
        "application/json"
      ]
    },
    {
      "name": "RFC7231 example, q=1 has an equal chance of negotiation (one of two options)",
      "supported": [
        "text/plain; q=0.5",
        "text/html",
        "text/x-dvi; q=0.8",
        "text/x-c"
      ],
      "acceptHeader": "text/html",
      "negotiated": [
        "text/html",
        "text/x-c"
      ]
    },
    {
      "name": "RFC7231 example, q=1 has an equal chance of negotiation (one of two options)",
      "supported": [
        "text/plain; q=0.5",
        "text/html",
        "text/x-dvi; q=0.8",
        "text/x-c"
      ],
      "acceptHeader": "text/x-c",
      "negotiated": [
        "text/html",
        "text/x-c"
      ]
    },
    {
      "name": "RFC7231 example, q=1 is selected (one of two options)",
      "supported": [
        "text/plain; q=0.5",
        "text/html",
        "text/x-dvi; q=0.8",
        "text/x-c"
      ],
      "acceptHeader": "text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c",
      "negotiated": [
        "text/html",
        "text/x-c"
      ]
    },
    {
      "name": "RFC7231 example, q=1 is selected is requested alongside lower quality",
      "supported": [
        "text/plain; q=0.5",
        "text/html",
        "text/x-dvi; q=0.8",
        "text/x-c"
      ],
      "acceptHeader": "text/html, text/x-dvi; q=0.8",
      "negotiated": [
        "text/html"
      ]
    },
    {
      "name": "RFC7231 example, a lower quality media type can be selected",
      "supported": [
        "text/plain; q=0.5",
        "text/html",
        "text/x-dvi; q=0.8",
        "text/x-c"
      ],
      "acceptHeader": "text/x-dvi; q=0.8",
      "negotiated": [
        "text/x-dvi"
      ]
    },
    {
      "name": "RFC7231 example, the lowest quality media type can be resolved",
      "supported": [
        "text/plain; q=0.5",
        "text/html",
        "text/x-dvi; q=0.8",
        "text/x-c"
      ],
      "acceptHeader": "text/plain; q=0.5",
      "negotiated": [
        "text/plain"
      ]
    }
  ]
}
