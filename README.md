# Content Negotiation Test Cases

Test cases for performing [server-driven content negotiation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Content_negotiation#server-driven_content_negotiation).

This is to make it easier for folks implementing the standard, as a way to better capture some of the edge cases that can occur.

## Format

The file `cases.json` has a schema documented in `cases-schema.json`.
